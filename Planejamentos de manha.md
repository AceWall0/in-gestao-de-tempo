## Plano para amanhã
Amanhã eu pretendo fazer um simples jogo de Tetris para me familiarizar com a biblioteca PIXI.js, para dar início a alguns outros projetos que eu tenho em mente no futuro.

Eu imagino que dê para fazer em um dia, mas vai depender de como serão as tarefas de amanhã também. I mean, geez, só essa tarefa de organizar tempo já tá me tomando mais tempo do que eu imaginei que levaria 🤷‍♂️


# Lista de tarefas
Eu poderia baixar os assets do Tetris, mas como pixel art é algo que eu eu quero treinar no processo, isso tem que entrar nos planos também:
- Desenhar/pintar os assets do tetris.
    - Início: 9h
    - Duração: 1h (2 pomodoros de 25 min)
- Codar, codar, codar com pixi.js
    - Início: quando der
    - Meta de duração: 3h (6 pomodoros de 25min, com um intervalo de 30 min). A meta tá meio baixa pra eu não me decepcionar caso eu falhe, mas a ideia é fazer mais que 5h, ou quem sabe mais! 

- 25 min de teclado, pra não perder o costume.  (Músicos profissionais me bateriam se vissem eu praticando tão pouco assim disso kkkk Mas ah, não é carreira que eu quero seguir.) 